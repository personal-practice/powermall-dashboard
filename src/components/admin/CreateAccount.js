import React, { useRef, useState } from 'react';
import { BsEye, BsEyeSlash } from 'react-icons/bs';
import useAuth from '../../hooks/useAuth';
import brand from '../../assets/images/brand.jpg';
import Swal from 'sweetalert2';

const CreateAccount = () => {

    const [eye, setEye] = useState(true);
    const emailRef = useRef('');
    const passwordRef = useRef('');
    const [role, setRole] = useState('');
    const { signUpWithEmailPassword, error, setError } = useAuth();

    const handleCreate = (e) => {
        e.preventDefault();

        if (role !== '') {
            const email = emailRef.current.value;
            const password = passwordRef.current.value;

            signUpWithEmailPassword(email, password, role);

            e.target.reset();
        } else {

            Swal.fire('Error', 'Please Select A Role', 'error');

        }

    };

    return (
        <div>

            <div className='container px-8 lg:px-0'>

                <div className='flex items-center justify-center lg:w-5/6 mx-auto' style={{ height: '87vh' }}>

                    <div className='border bg-white rounded-xl w-full lg:w-1/3 p-5 lg:p-10 flex flex-col gap-y-10'>

                        <div className='text-center mx-auto flex flex-col items-center gap-y-3'>
                            <img width="150px" src={brand} alt="brand" />
                        </div>

                        <form onSubmit={handleCreate} onChange={() => setError('')} className='flex flex-col gap-y-3'>

                            <div>
                                <label htmlFor="email">Email</label>
                                <input ref={emailRef} className='input' type="email" required />
                            </div>

                            <div>
                                <label htmlFor="password">Password</label>
                                <div className='relative'>

                                    <div onClick={() => setEye(!eye)} className='p-1 absolute right-3 top-5 cursor-pointer'>
                                        {
                                            eye ? <BsEyeSlash className='text-xl' /> : <BsEye className='text-xl' />
                                        }
                                    </div>
                                    <input ref={passwordRef} className='input' type={eye ? 'password' : 'text'} required />

                                </div>
                            </div>

                            {
                                error.length > 0 && <div><h1 className='text-red-600'>{error}</h1></div>
                            }

                            <div className='flex justify-between mt-2'>

                                <div className='flex items-center gap-x-1'>
                                    <input onChange={(e) => setRole(e.target.value)} type="radio" id="html" name="fav_language" value="Admin" />
                                    <label htmlFor="html">Admin</label>
                                </div>

                                <div className='flex items-center gap-x-1'>
                                    <input onChange={(e) => setRole(e.target.value)} type="radio" id="html" name="fav_language" value="Manager" />
                                    <label htmlFor="html">Manager</label>
                                </div>

                                <div className='flex items-center gap-x-1'>
                                    <input onChange={(e) => setRole(e.target.value)} type="radio" id="html" name="fav_language" value="Contributor" />
                                    <label htmlFor="html">Contributor</label>
                                </div>


                            </div>

                            <button className='submit_btn mt-4' type='submit'>Create Account</button>

                        </form>

                    </div>

                </div>

            </div >

        </div >
    );
};

export default CreateAccount;
