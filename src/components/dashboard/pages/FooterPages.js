import React from 'react';
import FooterPageEditor from './additional-pages/FooterPageEditor';

const FooterPages = () => {

    const pages = [

        { api: 'https://powermallapi.herokuapp.com/aboutus/61d4421094492be86a1eb5af', name: 'About Us' },
        { api: 'https://powermallapi.herokuapp.com/policy/61d45c84efb78ca1ca452200', name: 'Privacy & Policy' },
        // { api: 'https://powermallapi.herokuapp.com/cookie/61d4632c40713f746aa31e97', name: 'Cookie & Policy' },
        { api: 'https://powermallapi.herokuapp.com/shopwithus/61d46447e604446190359cb1', name: 'Why Shop With Us' },
        { api: 'https://powermallapi.herokuapp.com/termsandconditions/61d467a4c9aa6a166ac3962f', name: 'Terms And Conditions' },
        { api: 'https://powermallapi.herokuapp.com/shippinganddelivery/61d469c658f3d97276af25ef', name: 'Shipping And Delivery' },
        { api: 'https://powermallapi.herokuapp.com/paymentmethod/61d46b7189f9e716403924be', name: 'Payment Method' },

    ];

    return (

        <div>

            {
                pages.map((page) => (

                    <div key={page?.name}>
                        <FooterPageEditor api={page?.api} name={page?.name} />
                        <hr className='my-20' />
                    </div>

                ))
            }

        </div>

    );
};

export default FooterPages;
