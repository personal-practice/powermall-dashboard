import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import { MdOutlineClose } from 'react-icons/md';

export default function AlertDialog({ open, setOpen, text }) {


    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogActions>
                    <div onClick={handleClose} className='p-2 border rounded-full cursor-pointer'>
                        <MdOutlineClose className='text-xl text-red-700' />
                    </div>
                </DialogActions>

                <DialogContent>
                    <div className='text-lg flex flex-col gap-y-3'>
                        <div className='flex flex-col gap-y-2'>
                            <h1 className='font-light'><span className='font-medium mr-1'>Name:</span> {text?.name}</h1>
                            <h1 className='font-light'><span className='font-medium mr-1'>Subject:</span> {text?.subject}</h1>
                            <h1 className='font-light'><span className='font-medium mr-1'>Email:</span> {text?.email}</h1>
                        </div>
                        <div>
                            <h1 className='font-light'><span className='font-medium mr-1'>Message:</span> {text?.message}</h1>
                        </div>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    );
}
