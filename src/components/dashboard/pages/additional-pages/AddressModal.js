import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

const AddressModal = ({ address, setAddress, fulladdress }) => {

    const handleClose = () => {
        setAddress(false);
    };

    return (
        <div>

            <Dialog
                open={address}
                onClose={handleClose}
                sx={{ opacity: 0.5 }}
            >
                <DialogTitle id="alert-dialog-title">
                    {"Address"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {fulladdress}
                    </DialogContentText>
                </DialogContent>

            </Dialog>
        </div>
    );
};

export default AddressModal;
